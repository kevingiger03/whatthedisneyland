# whatthedisneyland

Le site WhatTheDisneyland est un site fan sur les parcs Disney du monde (ciblé Disneyland Paris pour le moment), où vous allez pouvoir retrouver toutes les infos à savoir sur le parc de votre choix. Attractions, temps d'attentes, catégories, etc...

## prototype

Un site qui va recenser l’état des attractions de Disneyland Paris, il va nous permettre d’avoir le temps d’attente habituel ainsi que le temps d’attente en direct dans le parc. Il va aussi pouvoir apporter quelques détails supplémentaires comme à qui est adressée cette attraction (sensations, famille, enfants, etc…), sa durée en secondes et un avis en étoiles des visiteurs du site
Lorsqu’on arrive sur la page d’accueil, on aura la possibilité d’accéder à la liste complète des attractions, ou directement de les préfiltrer par catégorie. Par la suite, sur cette liste nous pourront filtrer plus en détails les attractions avec les critères cités au point 1.


## communication

-	GET | Retourne la liste des temps d’attente et de la page info d’un parc
-	POST | Ajout d’un nouveau commentaire/avis sur une attraction | texte / adresse email ou pseudo
-	DELETE | Suppression d’un commentaire/avis sur une attraction | texte / adresse email ou pseudo
-	GET | Retourne la page accueil 
-	GET | Accéder à la page de login/inscription
